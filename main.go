package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"encoding/base64"
    "os"
)

func main() {

	// open database file
	db, err := sql.Open("sqlite3", "./mailslurper.db")
	errorCheck(err)
	defer db.Close()
	
	getAttachments(db)

}

func getAttachments(db *sql.DB) {
	row, err := db.Query("SELECT fileName, content FROM attachment")
	errorCheck(err)
	defer row.Close()
	
	var fileName, base64Content string

	for row.Next() { 
		row.Scan(&fileName, &base64Content)
		toPDF(fileName, base64Content)
	}
}

// convert to pdf
func toPDF(file, content string) {
	log.Println("decodging: ", file)
    dec, err := base64.StdEncoding.DecodeString(content)
    // errorCheck(err)

	log.Println("creating file: ", file)
    f, err := os.Create(file)
    errorCheck(err)
    defer f.Close()

	log.Println("writing file: ", file)
    if _, err := f.Write(dec); err != nil {
       panic(err)
    }

	log.Println("committing file: ", file)
    if err := f.Sync(); err != nil {
       panic(err)
    }
}

// simple nill errorcheck 
func errorCheck( err error) {
	if err != nil {
		log.Fatal(err)
	}
}